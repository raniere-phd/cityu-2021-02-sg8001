# CityU 2021-01 SG8001: Teaching Students: First Steps

Course Information page: http://www.cityu.edu.hk/catalogue/pg/202021/course/SG8001.htm

## Final Assessment

Individual Presentation (simulated teaching session) will be held in online format as well, but we will monitor further announcements from the Office of the Provost.

For the online assessment, you would need to please prepare a video 8 to 10 minutes presentation using your own recording device and submit it online via Canvas > Assignments. Please check it to make sure that the sound quality especially is good before submission. Students can teach ANY academic topic, including research topics, lecture topics or laboratory topics etc. and regard the audience as year 1 university students. The use of PowerPoint is required. The presentation should follow the OBTL framework with clear ILOs, TLAs, and ATs.

What to submit:

1. PowerPoint slide of the teaching topic;
2. The 8-10 minutes self-recorded video (teaching demonstration) based on the submitted PowerPoint slide. 
